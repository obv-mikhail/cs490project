document.getElementById("login_button").addEventListener("click", function(event) {
    event.preventDefault();
    var formData = new FormData(document.getElementById('login_form'));
    fetch("front.php", {method: 'POST', body: formData, credentials: 'same-origin'})
    .then((resp) => resp.json())
    .then((data) => {
        console.log(data);
        if(data["login_status"] !== "successful") {
            document.getElementById("login_status").innerHTML = "credentials unrecognized";
        } else {
            if(data["UserRole"] === "INSTRUCTOR") {
                window.location = "teacher.php";
            } else {
                window.location = "quiz.php";
            }
        }
    });
})
