
-- --------------------------------------------------------

--
-- Table structure for table `Answer`
--

CREATE TABLE IF NOT EXISTS `Answer` (
  `UserName` varchar(16) NOT NULL,
  `TestQuestionID` int(11) NOT NULL,
  `AnswerText` text,
  `AnswerFeedback` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE IF NOT EXISTS `Question` (
`QuestionID` int(11) NOT NULL,
  `QuestionDifficulty` text NOT NULL,
  `QuestionText` text NOT NULL,
  `QuestionFuncName` tinytext NOT NULL,
  `QuestionTopic` tinytext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `Question`
--

INSERT INTO `Question` (`QuestionID`, `QuestionDifficulty`, `QuestionText`, `QuestionFuncName`, `QuestionTopic`) VALUES
(1, 'medium', 'Write a function "fibonacci" that takes 1 integer parameter, n, which computes the fibonacci value for n.', 'fibonacci', 'numbers'),
(2, 'medium', 'Write a function named "palindrome" that takes 1 string parameter, s, and returns True or False if the word is a palindrome or not.', 'palindrome', 'strings'),
(3, 'easy', 'Write a function named "comparison" that takes 2 integer parameters, a and b, and returns 1 if a is larger than b, 0 if they are equal, and -1 if a is smaller than b.', 'comparison', 'numbers'),
(4, 'easy', 'Write a function named "divide" that takes 2 integer parameters, a and b, and returns a divided by b.', 'divide', 'numbers'),
(5, 'easy', 'Write a function named "multiply" that takes 2 integer parameters, a and b, and their product.', 'multiply', 'numbers'),
(6, 'easy', 'Write a function named "size" that 1 string parameter, and returns the size of the string. Use the for loop!', 'size', 'strings'),
(7, 'easy', 'Write a function named "bubbleSort", which uses the bubble-sort algorithm to sort a given list of integers. Use the for loop!', 'bubbleSort', 'numbers'),
(8, '', 'Write a function named "odd", which takes 1 integer parameter, and returns True if the integer is odd, and False if it is even.', 'odd', 'numbers'),
(9, 'easy', 'Write a function named "even", which takes 1 integer parameter, and returns True if the integer is even, and False if it is odd.', 'even', 'numbers'),
(10, 'easy', 'Write a function named "repeat", which takes 1 string parameter and 1 integer parameter, n, and prints the string n times. Use the while loop!', 'repeat', 'strings'),
(11, 'medium', 'Write a function named "vowelFreq", which takes 1 string parameter and returns the count of the number of vowels in that string. Use a for loop!', 'vowelFreq', 'strings'),
(12, 'medium', 'Write a function named "constanantFreq", which takes 1 string parameter and returns the count of the number of constanants in that string.', 'constanantFreq', 'strings'),
(13, 'medium', 'Write a function named "alphanumeric", which takes 1 string parameter and returns True if the string is alphanumeric, and False otherwise.', 'alphanumeric', 'strings'),
(14, 'medium', 'Write a function named "checkMaxSize", which takes 2 parameters, a list of strings and an integer, and returns True if the size of every string is less than the integer, and False otherwise.', 'checkMaxSize', 'strings'),
(15, 'medium', 'Write a function named "arraySum", which  takes 1 list parameter, and returns the sum of all the integers in the list. Hint: use a conditional for special cases!', 'arraySum', 'numbers'),
(16, 'medium', 'Write a function named "reverse", which  takes 1 string parameter, and returns the string in reverse order.', 'reverse', 'strings'),
(17, 'medium', 'Write a function named "minimum", which  takes 1 list parameter (that only contains integers), and returns the smallest integer in the array. Use a for loop!', 'minimum', 'numbers'),
(18, 'medium', 'Write a function named "maximum", which  takes 1 list parameter (that only contains integers), and returns the largest integer in the array. Use a for loop!', 'maximum', 'numbers');

-- --------------------------------------------------------

--
-- Table structure for table `QuestionConstraint`
--

CREATE TABLE IF NOT EXISTS `QuestionConstraint` (
`QuestionConstraintID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `QuestionConstraint` tinytext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `QuestionConstraint`
--

INSERT INTO `QuestionConstraint` (`QuestionConstraintID`, `QuestionID`, `QuestionConstraint`) VALUES
(1, 1, ''),
(2, 2, ''),
(3, 3, ''),
(4, 4, ''),
(5, 5, ''),
(6, 6, 'while'),
(7, 7, 'while'),
(8, 8, ''),
(9, 9, ''),
(10, 10, 'if'),
(11, 11, 'while'),
(12, 12, ''),
(13, 13, ''),
(14, 14, ''),
(15, 15, ''),
(16, 16, ''),
(17, 17, 'while'),
(18, 18, 'while');

-- --------------------------------------------------------

--
-- Table structure for table `QuestionTestcase`
--

CREATE TABLE IF NOT EXISTS `QuestionTestcase` (
`QuestionTestcaseID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `QuestionTestcaseArgs` tinytext NOT NULL,
  `QuestionTestcaseReturn` tinytext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `QuestionTestcase`
--

INSERT INTO `QuestionTestcase` (`QuestionTestcaseID`, `QuestionID`, `QuestionTestcaseArgs`, `QuestionTestcaseReturn`) VALUES
(3, 2, '(10, 10)', '0'),
(4, 2, '(3, 1)', '2'),
(5, 1, '(5)', '5'),
(6, 1, '(10)', '55'),
(7, 2, '("TACOCAT")', 'True'),
(8, 2, '("CS490")', 'False'),
(9, 3, '(4,3)', '1'),
(10, 3, '(1,90)', '-1'),
(11, 4, '(6,3)', '2'),
(12, 4, '(1,-1)', '-1'),
(13, 5, '(5,3)', '15'),
(14, 5, '(2,25)', '50'),
(15, 6, '("hello world!")', '12'),
(16, 6, '("four")', '4'),
(17, 7, '([4, 1, 2, 8, 10])', '[1, 2, 4, 8, 10]'),
(18, 7, '([-4, 10, 12, 0, 1])', '[-4, 0, 1, 10, 12]'),
(19, 8, '(9)', 'True'),
(20, 8, '(4)', 'False'),
(21, 9, '(9)', 'False'),
(22, 9, '(4)', 'True'),
(23, 10, '("Hello", 5)', 'Hello\nHello\nHello\nHello\nHello'),
(24, 10, '("CS490", 2)', 'CS490\nCS490'),
(25, 11, '("hello world")', '7'),
(26, 11, '("cs490")', '0'),
(27, 12, '("hello world")', '7'),
(28, 12, '("cs490")', '2'),
(29, 13, '("hello_world")', 'False'),
(30, 13, '("cs490")', 'True'),
(31, 14, '(["red", "blue", "green", "yellow", "purple"], 5)', 'False'),
(32, 14, '(["wood", "stone", "iron", "steel", "bronze"], 10)', 'True'),
(33, 15, '([4, 1, 2, 8, 10])', '25'),
(34, 15, '([-4, 10, "s", 0, 1])', '7'),
(35, 16, '("hello world")', 'dlorw olleh'),
(36, 16, '("cs490")', '094sc'),
(37, 17, '([4, 1, 2, 8, 10])', '1'),
(38, 17, '([-4, 10, 0, 1])', '0'),
(39, 18, '([4, 1, 2, 8, 10])', '10'),
(40, 18, '([-4, 15, 0, 1])', '15');

-- --------------------------------------------------------

--
-- Table structure for table `Test`
--

CREATE TABLE IF NOT EXISTS `Test` (
`TestID` int(11) NOT NULL,
  `TestName` tinytext NOT NULL,
  `TestFeedbackReleased` enum('TRUE','FALSE') NOT NULL,
  `TestTaken` enum('TRUE','FALSE') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Test`
--

INSERT INTO `Test` (`TestID`, `TestName`, `TestFeedbackReleased`, `TestTaken`) VALUES
(1, 'My Test', 'FALSE', 'FALSE');

-- --------------------------------------------------------

--
-- Table structure for table `TestQuestion`
--

CREATE TABLE IF NOT EXISTS `TestQuestion` (
`TestQuestionID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `TestID` int(11) NOT NULL,
  `TestQuestionPoints` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `UserName` varchar(16) NOT NULL,
  `UserRole` enum('INSTRUCTOR','STUDENT') NOT NULL,
  `UserPasswordHash` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`UserName`, `UserRole`, `UserPasswordHash`) VALUES
('user1', 'INSTRUCTOR', '$2y$10$jv5OD2aeDlxqMq8V8A/a4.lv0tgT9k1n79nFSqmQAfr9DLk9/j4k6'),
('user2', 'STUDENT', '$2y$10$jv5OD2aeDlxqMq8V8A/a4.lv0tgT9k1n79nFSqmQAfr9DLk9/j4k6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Question`
--
ALTER TABLE `Question`
 ADD PRIMARY KEY (`QuestionID`);

--
-- Indexes for table `QuestionConstraint`
--
ALTER TABLE `QuestionConstraint`
 ADD PRIMARY KEY (`QuestionConstraintID`);

--
-- Indexes for table `QuestionTestcase`
--
ALTER TABLE `QuestionTestcase`
 ADD PRIMARY KEY (`QuestionTestcaseID`);

--
-- Indexes for table `Test`
--
ALTER TABLE `Test`
 ADD PRIMARY KEY (`TestID`);

--
-- Indexes for table `TestQuestion`
--
ALTER TABLE `TestQuestion`
 ADD PRIMARY KEY (`TestQuestionID`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
 ADD UNIQUE KEY `UserName` (`UserName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Question`
--
ALTER TABLE `Question`
MODIFY `QuestionID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `QuestionConstraint`
--
ALTER TABLE `QuestionConstraint`
MODIFY `QuestionConstraintID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `QuestionTestcase`
--
ALTER TABLE `QuestionTestcase`
MODIFY `QuestionTestcaseID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `Test`
--
ALTER TABLE `Test`
MODIFY `TestID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `TestQuestion`
--
ALTER TABLE `TestQuestion`
MODIFY `TestQuestionID` int(11) NOT NULL AUTO_INCREMENT;