CREATE TABLE User (
UserName varchar(16) NOT NULL UNIQUE,
UserRole enum("INSTRUCTOR", "STUDENT") NOT NULL,
UserPasswordHash tinytext NOT NULL -- Hashing tbd in PHP with bcrypt
);

-- A hash for "test" password
INSERT INTO User VALUES ("user1", "INSTRUCTOR", "$2y$10$jv5OD2aeDlxqMq8V8A/a4.lv0tgT9k1n79nFSqmQAfr9DLk9/j4k6");
INSERT INTO User VALUES ("user2", "STUDENT", "$2y$10$jv5OD2aeDlxqMq8V8A/a4.lv0tgT9k1n79nFSqmQAfr9DLk9/j4k6");

CREATE TABLE Test (
TestID int NOT NULL AUTO_INCREMENT,
TestName tinytext NOT NULL,
TestFeedbackReleased enum("TRUE", "FALSE") NOT NULL,
TestTaken enum("TRUE", "FALSE") NOT NULL, /* can be in test because we have just one user */
PRIMARY KEY (TestID)
);

INSERT INTO Test VALUES ("1", "My Test", "FALSE", "FALSE");

/* Table showing a question in a question bank. */
CREATE TABLE Question (
QuestionID int NOT NULL AUTO_INCREMENT,
QuestionDifficulty text NOT NULL,
QuestionText text NOT NULL,
QuestionFuncName tinytext NOT NULL,
QuestionTopic tinytext NOT NULL,
PRIMARY KEY (QuestionID)
);

/* Very basic question for initial testing 
INSERT INTO Question VALUES ("1", "easy", "Write a function called add that will add two numbers and return the result.", "add");
Expected answer "def add(a, b):\n\treturn a+b" */

/* Very basic question for initial testing 
INSERT INTO Question VALUES ("2", "easy", "Write a function called subtract that will subtract a number from another number and return the result.", "subtract");
 Expected answer "def add(a, b):\n\treturn a-b" */



/* 
Table showing if a question belongs to some exam. 
Add multiple entries for a question if needed if present in multiple tests.
*/
CREATE TABLE TestQuestion (
TestQuestionID int NOT NULL AUTO_INCREMENT,
QuestionID int NOT NULL,
TestID int NOT NULL,
TestQuestionPoints text NOT NULL,
PRIMARY KEY (TestQuestionID)
);

/*
INSERT INTO TestQuestion (QuestionID, TestID, TestQuestionPoints) VALUES ("1", "1", "10");
INSERT INTO TestQuestion (QuestionID, TestID, TestQuestionPoints) VALUES ("2", "1", "10");
*/

CREATE TABLE QuestionTestcase (
QuestionTestcaseID int NOT NULL AUTO_INCREMENT,
QuestionID int NOT NULL,
QuestionTestcaseArgs tinytext NOT NULL,
QuestionTestcaseReturn tinytext NOT NULL,
PRIMARY KEY (QuestionTestcaseID)
);

/* 
INSERT INTO QuestionTestcase VALUES ("1", "1", "(10, 10)", "20");
INSERT INTO QuestionTestcase VALUES ("2", "1", "(3, 1)", "4");
INSERT INTO QuestionTestcase VALUES ("3", "2", "(10, 10)", "0");
INSERT INTO QuestionTestcase VALUES ("4", "2", "(3, 1)", "2");
*/

CREATE TABLE QuestionConstraint (
QuestionConstraintID int NOT NULL AUTO_INCREMENT,
QuestionID int NOT NULL,
QuestionConstraint tinytext NOT NULL,
PRIMARY KEY (QuestionConstraintID)
);

/*
Note that QuestionConstraint will be a JSON string array that the
middle end must decode and iterate over to get every required string
one by one.
*/

CREATE TABLE Answer (
UserName varchar(16) NOT NULL,
TestQuestionID int NOT NULL,
AnswerText text,
AnswerFeedback text /* Answer feeback will be provided with JSON instead of separate tables to reduce complexity. */
);