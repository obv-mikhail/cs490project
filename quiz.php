<?php
session_start();
if($_SESSION['ROLE'] != 'STUDENT'){
   header('Location: invalidaccess.html');
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Quiz</title>
<link rel="stylesheet" href="styles/all.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body class="box">
<div class="navbar">
<div style="display: flex;">
<a href="#" onclick="get_quiz()">Load Test Information</a>
<div style="flex: 1"></div>
<a href="index.html">Logout</a>
</div>
</div>
<div class="content" id="content">
</div>
<script>
get_quiz()
function get_quiz() {
  document.getElementById("content").innerHTML = `
    <form id="quiz" style="overflow-y: scroll; flex: 1;" onsubmit="return false;">
    </form>
  `;
  var formData2 = new FormData();
  formData2.append('request_type', 'get_feedback');
  fetch("front.php", {method: 'POST', body: formData2})
  .then((resp) => resp.json())
  .then((answers) => {
    console.log(answers);
    if(answers["TestTaken"] === "TRUE" && answers["TestFeedbackReleased"] === "FALSE") {
      document.getElementById("quiz").innerHTML = `
        <div>Test taken, but results not yet available.</div>
      `;
    } else if (answers["TestTaken"] === "FALSE") {

      var formData = new FormData();
      formData.append('request_type', 'get_quiz');
      fetch("front.php", {method: 'POST', body: formData})
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        var html_template = `<input type="hidden" name="request_type" value="store_answers">`;
        Object.entries(data).forEach(([test_question_id, question]) => {
          html_template += `
            <div>Difficulty: ${question['QuestionDifficulty']}
            <br>
            ${question['TestQuestionPoints']} POINTS
            </div>
            <div>${question['QuestionText']}</div>
            <div><textarea name="answers[${test_question_id}]" rows ="10" cols="50"></textarea></div>
          `;
        });
        document.getElementById("quiz").innerHTML = html_template;
        document.getElementById("content").innerHTML += `
        <div style="padding: 1rem; border-top: 2px solid #E0E0E0;">
          <button onclick="submit_quiz(event)">submit quiz</button>
        </div>
        `;
      });
    } else {
      var overall_grade = 0;
      var overall_grade_max = 0;
      var formData = new FormData();
      formData.append('request_type', 'get_quiz');
      fetch("front.php", {method: 'POST', body: formData})
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        var html_template = `<input type="hidden" name="request_type" value="store_answers">`;
        Object.entries(data).forEach(([test_question_id, question]) => {
          var feedback = JSON.parse(answers[test_question_id].AnswerFeedback);
          console.log(feedback)
          overall_grade += parseInt(feedback.feedback["All requirements."][0]);
          overall_grade_max += parseInt(feedback.feedback["All requirements."][1]);
          html_template += `
            <div class="spaced_box simple_border">${question['QuestionText']}</div>
            <div><textarea disabled rows ="10" cols="50">${answers[test_question_id].AnswerText}</textarea></div>
            <div class="spaced_box simple_border"><table>
            <tr>
            <th>requirement</th>
            <th>points given</th>
            <th>points max</th>
            <th>comments</th>
            </tr>
          `;
          Object.entries(feedback.feedback).forEach(([requirement, result]) => {
            html_template += `<tr>
              <td>${requirement}</td>
              <td>${result[0]}</td>
              <td>${result[1]}</td>
              <td>${result[2]}</td>
            </tr>`;
          });
          html_template += `
            </table></div>
          `;
        });
        html_template += `<div class="spaced_box simple_border">
          <b>Overall grade:</b> ${overall_grade / overall_grade_max * 100 }%
        </div>`;
        document.getElementById("quiz").innerHTML = html_template;
      });
    }
  });
}

function submit_quiz(event) {
  event.preventDefault();
  var formData = new FormData(document.getElementById('quiz'));
  fetch("front.php", {method: 'POST', body: formData})
  .then((resp) => resp.text())
  .then((data) => {
    console.log(data);
    var formData2 = new FormData();
    formData2.append('request_type', 'release_grades')
    fetch("front.php", {method: 'POST', body: formData2})
    .then((resp) => resp.text())
    .then((data) => {
      console.log(data);
      get_quiz();
    });
  });
}
</script>
</body>
</html>
