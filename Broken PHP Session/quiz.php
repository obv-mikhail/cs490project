<?php

session_start();
echo $_SESSION['ROLE'];
if($_SESSION['ROLE'] == 'STUDENT') {
  echo 'TRUE';
} else {
  echo 'FALSE';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Quiz</title>
<link rel="stylesheet" href="styles/all.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
<form id="quiz">
<input type="hidden" name="request_type" value="store_answers">
</form>
<div style="text-align: right;"><button id="submit_quiz">submit quiz</button></div>
<button onclick="get_quiz()">Take Quiz</button>
<button id="check_results">Check Results</button>
<div id="show_results"></div>
<script>
document.getElementById("submit_quiz").addEventListener("click", function(event) { // The submit button will convert the responses into a json object to send to the middle end
  event.preventDefault(); // prevent page from reloading
  // This should fillout the form data.
  var formData = new FormData(document.getElementById('quiz'));
  fetch("front.php", {method: 'POST', body: formData})
  .then((resp) => resp.text())
  .then((data) => {
    console.log(data)
  })
})

document.getElementById("check_results").addEventListener("click", function(event) {
  event.preventDefault();
  var getresults = false // placeholder, in reality I would like to fetch the results of the exam and display them here
  if(getresults) {
    document.getElementById('show_results').innerHTML = getresults
  } else {
    document.getElementById('show_results').innerHTML = 'Results not available'
  }
})

function get_quiz() {
  var feedback;
  var formData2 = new FormData();
  formData2.append('request_type', 'get_feedback');
  fetch("front.php", {method: 'POST', body: formData2})
  .then((resp) => resp.json())
  .then((data) => {
    console.log(data);
    feedback = data;
  });

  var formData = new FormData();
  formData.append('request_type', 'get_quiz');
  fetch("front.php", {method: 'POST', body: formData})
  .then((resp) => resp.json())
  .then((data) => {
    console.log(data);
    Object.entries(data).forEach(([test_question_id, question]) => {
      var grade = JSON.parse(feedback[test_question_id].AnswerFeedback).grade;
      // This needs to check if feeback has been provided already or not.
      document.getElementById("quiz").innerHTML += `
        <div><b>Grade:</b> ${grade}</div>
        <div>${question['QuestionText']}</div>
        <textarea name="answers[${test_question_id}]" rows ="10" cols="50"></textarea>
        <div><br></div>
      `;
    });
  });
}
</script>
</body>
</html>
