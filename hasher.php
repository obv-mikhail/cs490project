<?php
// Utility script for generating password hashes.
$password_hash = password_hash($_GET["password"], PASSWORD_DEFAULT);

echo "password_hash: " .  $password_hash . "<br>";
echo "hash match: " . password_verify($_GET["password"], $password_hash);

?>

