<?php 

// Will put useful functions in here for reducing the amount of boilerplate.
// Must put POSTFIELDS in $_POST
function very_simple_post_curl($my_url) {
    $ch = curl_init($my_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $ret_val = curl_exec($ch);
    curl_close($ch);
    return $ret_val;
}

?>