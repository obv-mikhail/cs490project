Login Accounts for Application:

Professor:
user1
test

Student:
user2
test

Professor can add to the question bank and search the bank via keyword
topic and difficulty

VERY IMPORTANT:
For test cases the parameters need to be in the form of (1,2,3,4). For example,
if the function adds two numbers, the test case parameter would be (1,2) and the expected
result would be 3. Must follow that format.

Once a question is added to the quiz board then the professor can adjust points
to the question if needed.

When the Quiz is submitted the student can login and take the quiz, all answers
must be in the form of functions according to the directions of the question.

When the student submits the quiz he/she cannot view the results until the professor
releases the score, the professor can adjust points wrongly graded by the server as well
as add comments before submitting.

Once the quiz score is released the student can login and view the results
