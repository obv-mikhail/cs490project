<?php 

require "helper_funcs.php";

$back_address = "https://web.njit.edu/~aa675/back.php";


ini_set('display_errors',1);
error_reporting(E_ALL);


// Release grades code.

if($_POST["request_type"] === "release_grades") {
    $_POST["request_type"] = "get_answers";
    $answers = json_decode(very_simple_post_curl($back_address), TRUE);
    $_POST["request_type"] = "get_quiz";
    $questions = json_decode(very_simple_post_curl($back_address), TRUE);
    foreach ($answers as $index => $answer) {
        $answers[$index]["testcases"] = array();
        $answers[$index]["constraints"] = array();
        $answers[$index]["feedback"] = array();

        $data_original = $answer["AnswerText"];
        $question = $questions[$answer["TestQuestionID"]];

        $func_name = $question["QuestionFuncName"];
        $testcases = $question["testcases"];

        $answers[$index]["grade"] = $max_unscaled_grade = count($question["testcases"]) + 1;
        
        $func_name_idx = addslashes("Function has the name \"" . $func_name . "\".");
        $func_name_pts = round(1 / $max_unscaled_grade * intval($question["TestQuestionPoints"]), 2);
        if(!preg_match("/def $func_name/", $data_original)) {
            $answers[$index]["feedback"][$func_name_idx] = array(0, $func_name_pts, "");
            $answers[$index]["grade"] -= 1;
            $data_original = preg_replace('/def ([a-z]\w+)/i', "def $func_name", $data_original);
        } else {
            $answers[$index]["feedback"][$func_name_idx] = array($func_name_pts, $func_name_pts, "");
        }

        $constraint_idx = "Function has " . $question["constraints"][0]["QuestionConstraint"];
        $constraint_pts = round(4 / $max_unscaled_grade * intval($question["TestQuestionPoints"]), 2);
        if(!empty($question["constraints"][0]["QuestionConstraint"])) {
            $max_unscaled_grade += 4;
            $constraint = $question["constraints"][0]["QuestionConstraint"];
            if(preg_match("/^\s*[^#]$constraint/m", $data_original)) {
                $answers[$index]["grade"] += 4;
                $answers[$index]["feedback"][$constraint_idx] = array($constraint_pts, $constraint_pts, "");
            } else {
                $answers[$index]["feedback"][$constraint_idx] = array(0, $constraint_pts, "");
            }
        }

        foreach ($testcases as $key => $value) {
            $data = $data_original . "\nprint(" . $func_name . $value["QuestionTestcaseArgs"] . ")";
            $fh = file_put_contents('test.py', $data);
            if ($fh === false) die();
            $output = trim(shell_exec("python test.py"));
            $testcase_idx = addslashes("Given the arguments " . $value["QuestionTestcaseArgs"] . " the function returns " . $value["QuestionTestcaseReturn"] . ".");
            $testcase_pts = round(1 / $max_unscaled_grade * intval($question["TestQuestionPoints"]), 2);
            if($output === $value["QuestionTestcaseReturn"]) {
                $answers[$index]["feedback"][$testcase_idx] = array($testcase_pts, $testcase_pts, "");
            } else {
                $answers[$index]["feedback"][$testcase_idx] = array(0, $testcase_pts, "");
                $answers[$index]["grade"]--;
            }
        }

        $answers[$index]["grade"] /= $max_unscaled_grade / intval($question["TestQuestionPoints"]);
        $answers[$index]["grade"] = round($answers[$index]["grade"], 2);
        $answers[$index]["feedback"]["All requirements."] = array($answers[$index]["grade"], intval($question["TestQuestionPoints"]), "");
    }
    $_POST["request_type"] = "store_feedback";
    $_POST["answers"] = $answers;
    echo very_simple_post_curl($back_address);
} else {
    echo very_simple_post_curl($back_address);
}

?>