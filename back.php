<?php
$servername = "sql1.njit.edu";


/*
ini_set('display_errors',1);
error_reporting(E_ALL);
*/

$db_user_and_db_name = "aa675";
$password = "PrJBEHSUq";


$db = new mysqli($servername, $db_user_and_db_name, $db_password, $db_user_and_db_name);

// Not going to use switch because in PHP it leads to more code than if - elif blocks.
// Code for getting all the questions for a quiz.
if($_POST["request_type"] === "get_quiz") {
    $sql = "SELECT * FROM Question, TestQuestion WHERE TestID='1' AND Question.QuestionID=TestQuestion.QuestionID"; // Test # hardcoded for now, while testing.
    $result = $db->query($sql);
    $question_array = array();
    while($row = $result->fetch_assoc()) {
        $question_id = $row["QuestionID"];

        $row["testcases"] = array();
        $sql = "SELECT * FROM QuestionTestcase WHERE QuestionID='$question_id'";
        $result2 = $db->query($sql);
        while($row2 = $result2->fetch_assoc()) $row["testcases"][] = $row2;

        $row["constraints"] = array();
        $sql = "SELECT * FROM QuestionConstraint WHERE QuestionID='$question_id'";
        $result2 = $db->query($sql);
        while($row2 = $result2->fetch_assoc()) $row["constraints"][] = $row2;

        $question_array[$row["TestQuestionID"]] = $row;
    }
    echo json_encode($question_array);
} elseif($_POST["request_type"] === "get_question") {
    $sql = "SELECT * FROM Question WHERE Question.QuestionID"; // Test # hardcoded for now, while testing.
    $result = $db->query($sql);
    $question_array = array();
    while($row = $result->fetch_assoc()) {
        $question_id = $row["QuestionID"];

        $row["testcases"] = array();
        $sql = "SELECT * FROM QuestionTestcase WHERE QuestionID='$question_id'";
        $result2 = $db->query($sql);
        while($row2 = $result2->fetch_assoc()) $row["testcases"][] = $row2;

        $row["constraints"] = array();
        $sql = "SELECT * FROM QuestionConstraint WHERE QuestionID='$question_id'";
        $result2 = $db->query($sql);
        while($row2 = $result2->fetch_assoc()) $row["constraints"][] = $row2;

        $question_array[$row["QuestionID"]] = $row;
    }
    echo json_encode($question_array);
} elseif($_POST["request_type"] === "store_question") {  
    $difficulty = $_POST["difficulty"];
    $text = $_POST["text"];
    $func = $_POST["func_name"];
    $topic = $_POST["question_topic"];
    $constraints = $_POST["constraints"];
    $testcases = array_combine($_POST["param"], $_POST["return"]);

    $sql = "INSERT INTO Question (QuestionDifficulty, QuestionText, QuestionFuncName, QuestionTopic)
    VALUES ('$difficulty', '$text', '$func', '$topic')";
    $db->query($sql);
    $id = $db->insert_id;
    $sql = "INSERT INTO QuestionConstraint (QuestionID, QuestionConstraint)
    VALUES ('$id', '$constraints')";
    $db->query($sql);
    foreach ($testcases as $input => $output) {
        $sql = "INSERT INTO QuestionTestcase (QuestionID, QuestionTestcaseArgs, QuestionTestcaseReturn) 
        VALUES ('$id', '$input', '$output')";
        $db->query($sql);
    }
} elseif($_POST["request_type"] === "store_answers") {
    echo json_encode($_POST["answers"]);
    $sql = "UPDATE Test
    SET TestTaken='TRUE'
    WHERE TestID='1'";
    $db->query($sql);
    foreach ($_POST["answers"] as $question_id => $value) {
        $sql = "DELETE FROM Answer WHERE UserName='user1' AND TestQuestionID='$question_id'";
        $db->query($sql);
        $sql = "INSERT INTO Answer (UserName, TestQuestionID, AnswerText)
        VALUES ('user2', '$question_id', '$value')";
        $db->query($sql);
    }
} elseif($_POST["request_type"] === "get_answers") {
    // This is for middle to get answers and grade them.
    $sql = "SELECT * FROM Answer, TestQuestion WHERE Answer.TestQuestionID=TestQuestion.TestQuestionID"; // Fetches all answers right now but should fetch a specific test later.
    $result = $db->query($sql);
    $answers_array = array();
    while($row = $result->fetch_assoc()) $answers_array[] = $row;
    echo json_encode($answers_array);
} elseif($_POST["request_type"] === "store_feedback") {
    foreach($_POST["answers"] as $index => $value) {
        $answer_feedback = json_encode(array("testcases" => $value["testcases"], "constraints" => $value["constraints"]
        , "grade" => $value["grade"], "feedback" => $value["feedback"]));
        echo $answer_feedback;
        $test_question_id = $value["TestQuestionID"];
        $sql = "UPDATE Answer
        SET AnswerFeedback='$answer_feedback'
        WHERE TestQuestionID='$test_question_id'";
        $db->query($sql);

    }
} elseif($_POST["request_type"] === "get_feedback") {
    $sql = "SELECT * FROM Test";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if($row["TestFeedbackReleased"] === "FALSE" || $row["TestTaken"] === "FALSE") {
        echo json_encode($row);
    } else {
        $sql = "SELECT * FROM Answer";
        $result = $db->query($sql);
        $answers_array = array();
        while($row = $result->fetch_assoc()) $answers_array[$row["TestQuestionID"]] = $row;
        echo json_encode($answers_array);
    }
} elseif($_POST["request_type"] === "get_feedback2") {
    $sql = "SELECT * FROM Test";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if($row["TestTaken"] === "FALSE") {
        echo "[\"FALSE\"]";
    } else {
        $sql = "SELECT * FROM Answer";
        $result = $db->query($sql);
        $answers_array = array();
        while($row = $result->fetch_assoc()) $answers_array[$row["TestQuestionID"]] = $row;
        echo json_encode($answers_array);
    }
} elseif($_POST["request_type"] === "new_quiz") {
    $sql = "UPDATE Test
    SET TestFeedbackReleased='FALSE', TestTaken='FALSE'
    WHERE TestID='1'";
    $result = $db->query($sql);
    $sql = "DELETE FROM TestQuestion WHERE TestID='1'";
    $db->query($sql);
    foreach($_POST["question_ids"] as $index => $value) {
        $points = $_POST["question_pts"][$value];
        $sql = "INSERT INTO TestQuestion (QuestionID, TestID, TestQuestionPoints) 
        VALUES ('$value', '1', '$points')";
        $db->query($sql);
    }
    echo json_encode($_POST);
} elseif($_POST["request_type"] === "provide_feedback") {
    echo json_encode($_POST);
    foreach($_POST["feedback"] as $index => $value) {
        $sql = "SELECT * FROM Answer WHERE Answer.TestQuestionID='$index'"; // Fetches all answers right now but should fetch a specific test later.
        $result = $db->query($sql);
        $row = $result->fetch_assoc();
        $feedback = json_decode($row["AnswerFeedback"], 1);
        foreach($value["grade"] as $index2 => $value2) {
            $feedback["feedback"][str_replace("%22", "\"", $index2)][0] = $value2;
        }
        foreach($value["comment"] as $index2 => $value2) {
            $feedback["feedback"][str_replace("%22", "\"", $index2)][2] = $value2;
        }
        $feedback2 = array();
        foreach($feedback["feedback"] as $index2 => $value2) {
            $feedback2[addslashes($index2)] = $value2;
        }
        $feedback["feedback"] = $feedback2;
        $feedback = json_encode($feedback);
        $sql = "UPDATE Answer
        SET AnswerFeedback='$feedback'
        WHERE Answer.TestQuestionID='$index'";
        $db->query($sql);
    }
    $sql = "UPDATE Test
    SET TestFeedbackReleased='TRUE'
    WHERE TestID='1'";
    $result = $db->query($sql);
} else {
    $username = $_POST['user'];
    $password = $_POST['pass'];

    $sql = "SELECT * FROM User WHERE UserName='$username'";
    $result = $db->query($sql);

    $login_status = array("login_status" => "unsuccessful");

    if ($row = $result->fetch_assoc()) if (password_verify($password, $row["UserPasswordHash"])) {
        $login_status["login_status"] = "successful";
        $login_status["UserRole"] = $row["UserRole"];
    }
    echo json_encode($login_status);
}
$db->close();
?>
