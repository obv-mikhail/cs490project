<?php
session_start();
if($_SESSION['ROLE'] != 'INSTRUCTOR'){
   header('Location: invalidaccess.html') ;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<link rel="stylesheet" href="styles/all.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body class="box">
<div class="navbar">
<div style="display: flex;">
<a href="#" onclick="create_test()">Manage Questions</a>
<a href="#" onclick="get_quiz()">Release Scores</a>
<div style="flex: 1"></div>
<a href="index.html">Logout</a>
</div>
</div>
<div class="content" id="content">
</div>
</body>
<script>
var testcasenum = 0;
create_test()
function submit_quiz() {
  console.log(testcasenum)
  for(var count =0; count < testcasenum; count++){
    console.log(document.getElementsByName(`param[${count}]`)[0].value)
    if(!document.getElementsByName("text")[0].value){
      alert('Please fill in question field!')
      return
    }
    if(!document.getElementsByName("func_name")[0].value){
      alert("Please fill in function name!")
      return
    }
    if(!document.getElementsByName(`param[${count}]`)[0].value){
      alert('Please fill in testcase fields!')
      return
    }
    if(!document.getElementsByName(`return[${count}]`)[0].value){
      alert('Please fill in parameter fields!')
      return
    }

  }
  var formData = new FormData(document.getElementById('get_quiz'));
  fetch("front.php", {method: 'POST', body: formData})
  .then((resp) => resp.text())
  .then((data) => {
    console.log(data);
    document.getElementById("content").innerHTML = `
      <div style="margin: 1rem;">Question added succesfully.</div>
    `;
  });
}

function add_testcase() {
  const wrapper = document.createElement('div');
  wrapper.id = `testcase_wrapper_${testcasenum}`;
  wrapper.style = "display: flex;"
  wrapper.innerHTML = `
  <input type="text" name="param[${testcasenum}]" placeholder="arguments" style="margin-right: 0.5rem;"></input>
  <input type="text" name="return[${testcasenum}]" placeholder="expected output" style="margin-left: 0.5rem;"></input>
  `;


  document.getElementById("testcase_fieldset").appendChild(wrapper);
  testcasenum++;
}
function remove_testcase(){
  if(testcasenum-1 == 0){
    alert("You cannot submit with 0 testcases!")
  } else{
    const node = document.getElementById(`testcase_wrapper_${--testcasenum}`);
    node.parentElement.removeChild(node);
  }
}
function create_test() {
  testcasenum = 0;
  document.getElementById("content").innerHTML = `
    <div class="teacher_page" style="flex: 1">
      <div id="left" style="display: flex; flex-direction: column;"></div>
      <div id="right" style="border-left: 2px solid #E0E0E0; display: flex; flex-direction: column;"></div>
      <div id="selections" style="border-left: 2px solid #E0E0E0; display: flex; flex-direction: column;"></div>
    </div>
  `;
  document.getElementById("left").innerHTML = `
    <form id="get_quiz" style="flex: 1;">
    <input type="hidden" name="request_type" value="store_question">
    <div><textarea name="text" rows="4" placeholder="question text"></textarea></div>
    <div><input type="text" name="func_name" placeholder="function name"></div>
    <div><input type="text" name="question_topic" placeholder="question topic"></div>
    <div class="spaced_box simple_border">
    <div>Constraints</div>
    <div>
    <input type='radio' name="constraints" value="if"> If statement</input>
    <input type='radio' name="constraints" value="while"> While loop</input>
    <input type='radio' name="constraints" value="for"> For loop</input>
    </div>
    </div>
    <div class="spaced_box simple_border">
    <div>Difficulty</div>
    <div>
    <input type='radio' name="difficulty" value="easy">Easy</input>
    <input type='radio' name="difficulty" value="medium">Medium</input>
    <input type='radio' name="difficulty" value="hard">Hard</input>
    </div>
    </div>
    <div id="testcase_fieldset" class="spaced_box simple_border">
    <div>Test Cases</div>
    </div>
    </form>
    <div style="padding: 1rem; border-top: 2px solid #E0E0E0; background: #424242;">
    <button type="submit" onclick="submit_quiz()" style="margin-right: 1rem;">submit question</button>
    <button type="button" onclick="add_testcase()" style="margin-right: 1rem;">add test case</button>
    <button type="button" onclick="remove_testcase()">remove test case</button>
    </div>
  `;
  document.getElementById("selections").innerHTML = `
  <form id='qbankselect' style="overflow-y: scroll; flex: 1;"><input type="hidden" name="request_type" value="new_quiz"></form>
  <div style="padding: 1rem; border-top: 2px solid #E0E0E0; background: #424242;">
  <div style="display: flex;">
  <button onclick="submit_quiz2(event)">make quiz</button>
  </div>
  </div>

  `;
  add_testcase();
  add_testcase();
  view_qbank();
}

function view_qbank(query='None',select='all') {
  var formData = new FormData();
  formData.append('request_type', 'get_question');
  fetch("front.php", {method: 'POST', body: formData})
  .then((resp) => resp.json())
  .then((data) => {
    console.log(data);
    document.getElementById("right").innerHTML = `
      <form id='qbank' style="overflow-y: scroll; flex: 1;" onsubmit="return false;"></form>
    `;
    // query == false || question['QuestionDifficulty'].includes(query) || question['QuestionTopic'].includes(query)
      Object.entries(data).forEach(([test_question_id, question]) => {
        if((select == 'all' && query == 'None') || (select == 'all' && query == false) || (question['QuestionDifficulty'].includes(select) && query == false) || (select == 'all' && question['QuestionTopic'].includes(query)) || (question['QuestionDifficulty'].includes(select) && question['QuestionTopic'].includes(query)) || (question['QuestionText'].includes(query))) {
          document.getElementById("qbank").innerHTML += `
          <div class="spaced_box simple_border">

              <div id="image" style="float: right; cursor: pointer;" onclick="addques(this, ${test_question_id});">
                <span style="font-size: large;">➕</span>
              </div>
              <div style="margin-right: 1rem;">Difficulty:<b> ${question['QuestionDifficulty']}</b></div>
              <div>Topic:<b> ${question['QuestionTopic']}</b></div>

            <div style="display: none; ">
              <input type="checkbox" name="question_ids[]" value="${test_question_id}" style="display: none" checked>
              <input id="question_pts[${test_question_id}]" type="text" placeholder="question points" name="question_pts[${test_question_id}]" value="10">
            </div>
            <div>${question['QuestionText']}</div>
          </div>
          `;
        }
      });
      document.getElementById("qbank").innerHTML += `
        <input type="hidden" name="request_type" value="new_quiz">
      `;
      document.getElementById("right").innerHTML += `
        <div style="padding: 1rem; border-top: 2px solid #E0E0E0;background: #424242;">
        <div style="display: flex;">
        <input type="text" id="entry" placeholder="keyword" style="margin-right: 1rem;"></input>
        <select id="mySelect" style="margin-right: 1rem; margin-top: +1%;">
        <option value="all">all</option>
        <option value="easy">easy</option>
        <option value="medium">medium</option>
        <option value="hard">hard</option>
        </select>
        <button onclick="view_qbank(document.getElementById('entry').value,document.getElementById('mySelect').value)">search</button>
        </div>
        </div>
      `;
  });

}

document.addEventListener('keydown', function(event) {
        if (event.keyCode == 13) {
            view_qbank(document.getElementById('entry').value,document.getElementById('mySelect').value)
        }
    });

function addques(elem, test_question_id) {
  const e = elem.parentNode;
  elem.innerHTML = `
    <span style="font-size: large;">🗑</span>
  `;
  elem.setAttribute('onclick', 'removeques(this)');
  document.getElementById("qbankselect").appendChild(e);
  document.getElementById(`question_pts[${test_question_id}]`).parentElement.style.display = "flex";
}

function removeques(node){
  const parent = node.parentElement;
  parent.parentElement.removeChild(parent);
  return;
}

function submit_quiz2(event) {
  event.preventDefault();
  var formData = new FormData(document.getElementById('qbankselect'));
  fetch("front.php", {method: 'POST', body: formData})
  .then((resp) => resp.text())
  .then((data) => {
    console.log(data);
    document.getElementById("content").innerHTML = `
      <div style="margin: 1rem;">Test created succesfully.</div>
    `;
  });
}

function get_quiz() {
  var formData2 = new FormData();
  formData2.append('request_type', 'get_feedback2');
  fetch("front.php", {method: 'POST', body: formData2})
  .then((resp) => resp.json())
  .then((answers) => {
    console.log(answers);
    if(!Array.isArray(answers)) {
      var overall_grade = 0;
      var overall_grade_max = 0;
      var formData = new FormData();
      formData.append('request_type', 'get_quiz');
      fetch("front.php", {method: 'POST', body: formData})
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        var html_template = `<form id="feedback_form" style="overflow-y: scroll; flex: 1;"><input type="hidden" name="request_type" value="provide_feedback">`;
        Object.entries(data).forEach(([test_question_id, question]) => {
          var feedback = JSON.parse(answers[test_question_id].AnswerFeedback);
          console.log(feedback);
          overall_grade += parseInt(feedback.feedback["All requirements."][0]);
          overall_grade_max += parseInt(feedback.feedback["All requirements."][1]);
          html_template += `
            <div class="spaced_box simple_border">${question['QuestionText']}</div>
            <div><textarea disabled rows ="10" cols="50">${answers[test_question_id].AnswerText}</textarea></div>
            <div class="spaced_box simple_border"><table>
            <tr>
            <th>requirement</th>
            <th>points given</th>
            <th>points max</th>
            <th>comments</th>
            </tr>
          `;
          Object.entries(feedback.feedback).forEach(([requirement, result]) => {
            html_template += `<tr>
              <td>${requirement}</td>
              <td><input type="text" value="${result[0]}" style="margin-right: 0.5rem;" name='feedback[${test_question_id}][grade][${requirement}]'></td>
              <td>${result[1]}</td>
              <td><input type="text" value="${result[2]}" style="margin-right: 0.5rem;" name='feedback[${test_question_id}][comment][${requirement}]'></td>
            </tr>`;
          });
          html_template += `
            </table></div>
          `;
        });
        html_template += `
        <hr>
        <div class="spaced_box simple_border"><b>Overall grade: ${overall_grade / overall_grade_max * 100 }%</b></div>
        </form>
        <div style="padding: 1rem; border-top: 0.12rem solid #E0E0E0; background: #424242;">
        <div style="display: flex;">
        <button onclick="provide_feedback(event)">release</button>
        </div>
        </div>
        `;
        document.getElementById("content").innerHTML = html_template;
      });
    } else {
      document.getElementById("content").innerHTML = `
        <div style="margin: 1rem;">Test not yet taken.</div>
      `;
    }
  });
}

function provide_feedback() {
  var formData = new FormData(document.getElementById('feedback_form'));
  fetch("front.php", {method: 'POST', body: formData})
  .then((resp) => resp.json())
  .then((data) => {
    console.log(data);
    document.getElementById("content").innerHTML = `
      <div style="margin: 1rem;">Scores released successfully.</div>
    `;
  });
}
</script>
</html>
